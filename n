Mappings:
  AWSInstanceType2Arch:
    t1.micro:
      Arch: PV64
  AWSInstanceType2NATArch:
    t1.micro:
      Arch: NATPV64
  AWSRegionArch2AMI:
    us-east-1:
      PV64: ami-2a69aa47
      HVM64: ami-6869aa05
      HVMG2: ami-50b4f047
